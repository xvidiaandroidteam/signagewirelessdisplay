
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-keepattributes *Annotation*
-libraryjars <java.home>/lib/rt.jar(java/**,javax/**)

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-ignorewarnings

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class * implements java.io.Serializable
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.ListFragment
-keep public class android.content.** { *; }
-dontwarn com.android.volley.**
-dontskipnonpubliclibraryclassmembers

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
-keepclassmembers class **.R$* {
    public static <fields>;
}
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keepattributes *Annotation*,EnclosingMethod,Signature
-keep class com.fasterxml.jackson.databind.**{ *; }
-keep class com.fasterxml.jackson.core.**{ *; }
-keep class com.fasterxml.jackson.annotations.**{ *; }
-keepnames class com.fasterxml.jackson.** { *; }
 -dontwarn com.fasterxml.jackson.databind.**
 -keep class org.codehaus.** { *; }
 -keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
 public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *;
 }
-keep public class your.class.** {
  public void set*(***);
  public *** get*();
}

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}

-keep class org.simpleframework.**{ *; }
-keep class org.simpleframework.xml.**{ *; }
-keep class org.simpleframework.xml.core.**{ *; }
-keep class org.simpleframework.xml.util.**{ *; }
-keep class org.simpleframework.xml.stream.**{ *; }
-keep class org.simpleframework.xml.**{ *; }

-keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keep public class in.com.app.model.** {
  public void set*(***);
  public *** get*();
}
-keep class in.com.app.model.**{ *; }
-keepclassmembernames class in.com.app.model.** { *; }
-keepclasseswithmembers class in.com.app.model.** { *;}

-keep public class in.com.app.domain.** {
  public void set*(***);
  public *** get*();
}
-keep class in.com.app.domain.**{ *; }
-keepclassmembernames class in.com.app.domain.** { *; }
-keepclasseswithmembers class in.com.app.domain.** { *;}
#-keep public class org.simpleframework.xml.stream..Format**{ *; }
-keepattributes ElementList, Root

-keepclassmembers class * {
    @org.simpleframework.xml.* *;
}

-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
#-keepclassmembers allowobfuscation class * {
#    @org.simpleframework.xml.* <fields>;
#    @org.simpleframework.xml.* <init>(...);
#}
#-keep interface org.simpleframework.xml.core.Label {
#   public *;
#}
#-keep class * implements org.simpleframework.xml.core.Label {
#   public *;
#}
#-keep interface org.simpleframework.xml.core.Parameter {
#   public *;
#}
#-keep class * implements org.simpleframework.xml.core.Parameter {
#   public *;
#}
#-keep interface org.simpleframework.xml.core.Extractor {
#   public *;
#}
#-keep class * implements org.simpleframework.xml.core.Extractor {
#   public *;
#}
#-keep class * implements org.simpleframework.xml.ElementList {
#    public *;
# }