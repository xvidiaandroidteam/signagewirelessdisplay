package in.com.app.release.view;

public interface IVideoCompleteListener {

    void onVideoCompleted();
}