package in.com.app.release.domain;
import org.simpleframework.xml.Root;

/**
 * This class serialises xml for layout scheduled
 * @author Ravi@xvidia
 * @since version 1.0
 *
 */

@Root
class File {
    private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	   
	
}
