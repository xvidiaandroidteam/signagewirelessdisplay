package in.com.app.release;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import in.com.app.release.BroadCastReceivers.SyncLayoutReceiver;
import in.com.app.release.data.LogData;
import in.com.app.release.sharefile.ReceiverActivity;
import in.com.app.release.utility.MyExceptionHandler;
import in.com.app.release.wifiDirect.WiFiServiceDiscoveryActivity;

public class Splash extends Activity{
private int currentApiVersion;
private static Context ctx;
//String user_name = System.getProperty("user.name", "n/a");
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		try{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		  currentApiVersion = android.os.Build.VERSION.SDK_INT;

		    final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
		        | View.SYSTEM_UI_FLAG_FULLSCREEN
		        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		    // This work only for android 4.4+
		    if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
		    {
		        getWindow().getDecorView().setSystemUiVisibility(flags);
		        final View decorView = getWindow().getDecorView();
		        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
		            {

		                @Override
		                public void onSystemUiVisibilityChange(int visibility)
		                {
		                    if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
		                    {
		                        decorView.setSystemUiVisibility(flags);
		                    }
		                }
		            });
		    }
		} catch(Exception e){
//			e.printStackTrace();
		}
		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,AppMain.class));
		setContentView(R.layout.layout_splash);
		ctx = this;
		try {
				updateNewFolderStructure();
//			LogData.getInstance().setScreenCastEnabled(AppMain.getAppMainContext(), false);
//				start();
//				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//				Date now = new Date();
//				String timeNew = formatter.format(now);
//				String time = LogData.getInstance().getOntimeId(ctx);
//				if(time==null){
//					time = timeNew;
//				}else if(time.isEmpty()){
//					time = timeNew;
//				}
//				LogData.getInstance().setOntimeId(ctx, time);
//				String id = LogData.getInstance().getAppID(ctx)+"_"+"BOX"+"_"+now.getTime();
//				OnOffTimeData object = new OnOffTimeData();
//		    	object.setId(id);
//		    	object.setBoxId(LogData.getInstance().getAppID(ctx));
//		    	object.setOnTime(LogData.getInstance().getOntimeId(ctx));
//		    	object.setSavingTime(time);
//		    	object.setOffTime("");
////		    	Log.e("BOOTRECIEVR",time);
////		    	object.setBoxId(LogData.getInstance().getAppID(context));
//		    	DataCacheManager.getInstance(ctx).saveOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_BOX_TABLE, object);
		 
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if(!LogData.getInstance().getScreenCastEnabled(MyApplication.getAppContext())){
//						Toast.makeText(Splash.this, "getScreenCastEnabled no", Toast.LENGTH_SHORT).show();
						startApp();
					}else if(!isAppRunningInBackground()){
						launchWfdApplication();
					}else{
//						Toast.makeText(Splash.this, "getScreenCastEnabled yes", Toast.LENGTH_SHORT).show();
						startApp();
					}
				}
			}, 2000);
		} catch(Exception e){
//			e.printStackTrace();
		}
	}
@Override
public void onWindowFocusChanged(boolean hasFocus)
{
    super.onWindowFocusChanged(hasFocus);
    if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
    {
        getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
	private void updateNewFolderStructure(){
		try{
			String destPath = Environment.getExternalStorageDirectory()
					+ AppState.DISPLAY_FOLDER;
			String sourcePath = Environment.getExternalStorageDirectory()
					+ AppState.OLD_DISPLAY_FOLDER;
			 File sourceLocation = new File (sourcePath);
			 File targetLocation = new File (destPath);
			 if (sourceLocation.isDirectory() && sourceLocation.exists()) {		 
				 try {
					FileManager.copyDirectoryOldToNew(sourceLocation, targetLocation);		
					FileManager.deleteDir(sourceLocation);
				 } catch (IOException e) {
					 e.printStackTrace();
					}
			 }
		} catch(Exception e){
//			e.printStackTrace();
		}
	}
	private void startApp(){
		Intent i = new Intent(Splash.this, ReceiverActivity.class);
		startActivity(i);
		finish();
	}

	private boolean isAppRunningInBackground() {
		boolean retValue = false;
		final ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);

		List<ActivityManager.RunningAppProcessInfo> mTasks = am.getRunningAppProcesses();

		for (int i = 0; i < mTasks.size(); i++) {
			String name = mTasks.get(i).processName;

			if (name.equals(AppState.wifiDisplayPackage)) {
				retValue = true;
				break;
			}
		}
		return retValue;
	}
	/**
	 * Launch Another application
	 */
	void launchWfdApplication() {
		try {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {

					setSyncLayoutAlarm(MyApplication.getAppContext());
				}
			}, 1500);
			Intent intent = getPackageManager().getLaunchIntentForPackage(AppState.wifiDisplayPackage);
			if (intent != null) {
				Toast.makeText(Splash.this, "WFD LAUNCH", Toast.LENGTH_SHORT).show();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}

		} catch (ActivityNotFoundException e) {
		} catch (Exception e) {
		}
	}
	@Override
	public void onBackPressed() {
		
	}

	private void setSyncLayoutAlarm(Context context) {
		Calendar updateTime = Calendar.getInstance();
		Intent downloader = new Intent(context, SyncLayoutReceiver.class);
		PendingIntent recurringDownload = PendingIntent.getBroadcast(context,
				0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				updateTime.getTimeInMillis(),
				4000, recurringDownload);
//		alarms.setExact(AlarmManager.RTC_WAKEUP,updateTime.getTimeInMillis(), recurringDownload);

	}
}
