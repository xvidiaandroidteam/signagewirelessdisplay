package in.com.app.release;

/**
 * This class has static fields for server connections.
 * @author Ravi@Xvidia
 * @since version 1.0
 *
 */
public class ClientConnectionConfig {

//	static String _SERVERURL_IDEA = "http://192.168.10.134/signagedemo/xmds.php";
	private static final String _SERVERURL_IDEA = MyApplication.getAppContext().getResources().getString(R.string.signage_register_url);
	//	static String _SERVERURL_OLD = "http://54.254.103.114/xvidia/xmds.php";
//	public static final String _SERVERURL_AMOSTA = "http://dss.amosta.com/xmds.php";
//	static String _UNIQUE_SERVER_KEY_OLD = "pkb@bj1757";
//	static String _UNIQUE_SERVER_KEY_DEMO = "7f0hRO";
	//	static String _UNIQUE_SERVER_KEY_AMOSTA = "f9jbrl";
	private static final String _UNIQUE_SERVER_KEY_IDEA =MyApplication.getAppContext().getResources().getString(R.string.signage_register_key);
	//	static String _VERSION_OLD = "3";
	private static final String _VERSION_NEW = "4";
	public static final String _SERVERURL = _SERVERURL_IDEA;
	public static final String _UNIQUE_SERVER_KEY = _UNIQUE_SERVER_KEY_IDEA;
	public static String _HARDWAREKEY = "";
	public static String _DISPLAYNAME = "";
	public static String _ADDRESS = "";
	public static String _ASSETID = "";
	public static String LAST_REQUEST_DATA = "";
	public static final String _VERSION = _VERSION_NEW;
}
