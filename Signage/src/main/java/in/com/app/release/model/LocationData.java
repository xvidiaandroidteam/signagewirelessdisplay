package in.com.app.release.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LocationData extends DataParent{

    private String id = null;
    private String lat = null;
    private String longitude = null;

	public String getId() {
			return getValidatedString(id);
		}
		public void setId(String id) {
			this.id = id;
		}


		public String getLat() {
			return getValidatedString(lat);
		}
		public void setLat(String lat) {
			this.lat = lat;
		}
		public String getLongitude() {
			return getValidatedString(longitude);
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		
	}
