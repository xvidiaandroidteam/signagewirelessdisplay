package in.com.app.release.data;

import android.os.Handler;

import in.com.app.release.domain.DisplayLayout;
import in.com.app.release.model.MediaTimeData;


public class RefreshMediaData {
	private String duration = null;
	private long timestamp ;
	private DisplayLayout.Region region=null;
	private int mediaCount = 0;

	private int viewId = 0;
	private Handler callbackHandler;
	private Runnable callbackRunnable;
	private MediaTimeData mediaTimeData;

	public Runnable getCallbackRunnable() {
		return callbackRunnable;
	}
	public void setCallbackRunnable(Runnable callbackRunnable) {
		this.callbackRunnable = callbackRunnable;
	}

	public int getViewId() {
		return viewId;
	}
	public void setViewId(int viewId) {
		this.viewId = viewId;
	}

	public Handler getCallbackHandler() {
		return callbackHandler;
	}
	public void setCallbackHandler(Handler callbackHandler) {
		this.callbackHandler = callbackHandler;
	}

	public MediaTimeData getMediaTimeData() {
		return mediaTimeData;
	}
	public void setMediaTimeData(MediaTimeData mediaTimeData) {
		this.mediaTimeData = mediaTimeData;
	}
	
	public int getMediaCount() {
		return mediaCount;
	}
	public void setMediaCount(int mediaCount) {
		this.mediaCount = mediaCount;
	}

	public String getDuration() {
		return getValidatedString(duration);
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}

	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public DisplayLayout.Region getRegion() {
		return region;
	}
	public void setRegion(DisplayLayout.Region region) {
		this.region = region;
	}

	//	static Vector<MusicData> musicList;
	private String getValidatedString(String valStr){
		if(valStr == null){
			return "";
		}else{
			return valStr.trim();
		}
	}
	
	
	
}
