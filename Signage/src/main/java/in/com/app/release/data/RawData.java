package in.com.app.release.data;

/**
 * This class maintains information of the currently displayed downloaded file 
 * @author Ravi@xvidia
 *
 */
class RawData {

	private String type = "";
	private String filename = "";
	private byte[] dataBytes = null;
	private int dataLength = 0;
	private String id = "";
	private boolean dataSoredInDB = false;
	 public RawData(String idImage, String fileName, String fileType, byte[] bytes, int datalen){
		 id = idImage;
		 filename = fileName;
		 type = fileType;
		 dataBytes = bytes;		 
		 dataLength = datalen;
	 }
	 
	 public RawData(String idImage, String fileName, String fileType, int datalen){
		 id = idImage;
		 filename = fileName;
		 type = fileType;
		 dataBytes = null;		 
		 dataLength = datalen;
		 dataSoredInDB = true;
	 }
}
