package in.com.app.release.background;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import in.com.app.release.AppMain;
import in.com.app.release.ClientConnectionConfig;
import in.com.app.release.IDisplayLayout;
import in.com.app.release.storage.caching.sqlight.manager.DataCacheManager;
import in.com.app.release.wsdl.XMDS;

/**
 * This class is wrapper for a asyncTask that checks if STB is registered on server.
 * @author Ravi@Xvidia
 * @since version 1.0
 *
 */
class BackgroundServerConnectionManagerDisplay extends
AsyncTask<String, Void, String> implements IDisplayLayout {

	
	
	private Context context = null;
	private Activity activity = null;
	
	BackgroundServerConnectionManagerDisplay(Context ctx, Activity act){
		context = ctx;
		activity = act;
	}

	
	private final String TAG = getClass().getCanonicalName();
	
	
	
	@Override
	protected String doInBackground(String... params) {
		XMDS xmds = new XMDS(ClientConnectionConfig._SERVERURL);

		return xmds.RegisterDisplay(ClientConnectionConfig._UNIQUE_SERVER_KEY, ClientConnectionConfig._HARDWAREKEY,
				ClientConnectionConfig._DISPLAYNAME, ClientConnectionConfig._VERSION);
	}

	@Override
	protected void onPostExecute(String result) {			
		Log.d(TAG, "Got response " + result);
		boolean nextStepFlag = false;
		if (result!=null && result.contains("Display is active and ready to start")) {
			DataCacheManager.getInstance(AppMain.getAppMainContext()).saveSettingData(_KEY_XVIDIA_STATE_REG_COMPLETE, FLAG_TRUE);
			nextStepFlag = true;
		}
		try{
			
				StateMachineDisplay.gi(context, activity).initProcess(nextStepFlag, StateMachineDisplay.GETSCHEDULE);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}