package in.com.app.release.BroadCastReceivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView.BufferType;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import in.com.app.release.AppMain;
import in.com.app.release.IDisplayLayout;
import in.com.app.release.MyApplication;
import in.com.app.release.data.LogData;
import in.com.app.release.model.IAPIConstants;
import in.com.app.release.model.LayoutTimeData;
import in.com.app.release.model.MediaTimeData;
import in.com.app.release.model.OnOffTimeData;
import in.com.app.release.network.ServiceURLManager;
import in.com.app.release.network.VolleySingleton;
import in.com.app.release.storage.caching.sqlight.MySqliteHelper;
import in.com.app.release.storage.caching.sqlight.manager.DataCacheManager;
import in.com.app.release.utility.LogUtility;
import in.com.app.release.utility.NetworkUtil;

/**
 * This class extends {@link BroadcastReceiver} to update nework change information
 *
 * @author Ravi@xvidia
 * @since Version 1.0i7
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    private boolean hitRequest = false;
    boolean continueUpdate = false;

//    Vector<MediaTimeData> mList = new Vector<MediaTimeData>();
//    Vector<LayoutTimeData> mListLayout = new Vector<LayoutTimeData>();
//    Vector<OnOffTimeData> onOffListBox = new Vector<OnOffTimeData>();
//    Vector<OnOffTimeData> onOffListScreen = new Vector<OnOffTimeData>();
//    Vector<OnOffTimeData> onOffListAPP = new Vector<OnOffTimeData>();

    // boolean noRequestAgain = false;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.ENGLISH);
            Date now = new Date();
            String status = NetworkUtil.getConnectivityStatusString(context);
            if (status.equalsIgnoreCase("Not connected to Internet")) {
//		        	HeartBeatData.getInstance().setNeworkType(status);
                hitRequest = false;
                String data = "Time: " + formatter.format(now);
                LogData.getInstance().setInternetConnection(context, false);
                DataCacheManager.getInstance(context).saveSettingData(IDisplayLayout._KEY_XVIDIA_STATE_NETWORK_OFF_TIME, data);
            } else {
                String data = "Time: " + formatter.format(now);
                Log.d("NETWORK STATUS", "" + status);
                hitRequest = true;
                LogData.getInstance().setInternetConnection(context, true);
//		        	HeartBeatData.getInstance().setNeworkType(status);
                DataCacheManager.getInstance(context).saveSettingData(IDisplayLayout._KEY_XVIDIA_STATE_NETWORK_ON_TIME, data);
                DataCacheManager.getInstance(context).saveSettingData(IDisplayLayout._KEY_XVIDIA_STATE_NETWORK_TYPE, status);


            }
            if (hitRequest &&
                    LogData.getInstance().getRequestSend(context)) {

                LogData.getInstance().setRequestSend(context, false);
                try {
                    syncSavedListMediaLayoutData(context);
                } catch (Exception e) {
                }
//                boolean flag = LogData.getInstance().setNetworkType(context, status);
                AppMain.connectionStatus.setText(status, BufferType.NORMAL);
//                try {
//                    if (flag) {
////			        		sendLogRequest(context,LogUtility.getInstance().getNetworkJson(context),LogUtility.getNewtorkLogUrl());
//                    }
//
//                } catch (Exception e) {
//                }
            }
            //Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }
    }

    private void syncSavedMediaData(Context context) {
        try {
            Vector<MediaTimeData> mList = new Vector<MediaTimeData>();
            mList = DataCacheManager.getInstance(context).getAllMediaTimeData();


            if (mList != null && mList.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(mList);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_MEDIA_TIME_OFFLINE);
                sendSyncMediaRequest(context, url, jsonRequestString, mList);
            }
        } catch (Exception e) {

        }
    }

    private void syncSavedLayoutData(Context context) {
        try {
            Vector<LayoutTimeData> mListLayout = new Vector<LayoutTimeData>();

            mListLayout = DataCacheManager.getInstance(context).getAllLayoutTimeData();


            if (mListLayout != null && mListLayout.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(mListLayout);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_LAYOUT_TIME_OFFLINE);
                sendSyncLayoutRequest(context, url, jsonRequestString, mListLayout);
            }
        } catch (Exception e) {

        }
    }
    private void syncSavedListMediaLayoutData(Context context) {
        try {
            Vector<MediaTimeData> mList = new Vector<MediaTimeData>();
            Vector<LayoutTimeData> mListLayout = new Vector<LayoutTimeData>();
            Vector<OnOffTimeData> onOffListBox = new Vector<OnOffTimeData>();
            Vector<OnOffTimeData> onOffListScreen = new Vector<OnOffTimeData>();
            Vector<OnOffTimeData> onOffListAPP = new Vector<OnOffTimeData>();

            onOffListBox = DataCacheManager.getInstance(context).getAllOnOffTimeBoxData();
            onOffListAPP = DataCacheManager.getInstance(context).getAllOnOffTimeAppData();
            onOffListScreen = DataCacheManager.getInstance(context).getAllOnOffTimeScreenData();

            mListLayout = DataCacheManager.getInstance(context).getAllLayoutTimeData();
            mList = DataCacheManager.getInstance(context).getAllMediaTimeData();

            if (onOffListScreen != null && onOffListScreen.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(onOffListScreen);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_SCREEN_OFFLINE);
                sendSyncOnOffDataRequest(context, url, jsonRequestString, onOffListScreen);
//            continueUpdate = true;
            }
            if (onOffListBox != null && onOffListBox.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(onOffListBox);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_BOX_OFFLINE);
                sendSyncOnOffDataRequest(context, url, jsonRequestString, onOffListBox);
            }
            if (onOffListAPP != null && onOffListAPP.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(onOffListAPP);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_APP_OFFLINE);
                sendSyncOnOffDataRequest(context, url, jsonRequestString, onOffListAPP);
            }
            if (mList != null && mList.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(mList);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_MEDIA_TIME_OFFLINE);
                sendSyncMediaRequest(context, url, jsonRequestString, mList);
            }
            if (mListLayout != null && mListLayout.size() > 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonRequestString = null;
                try {
                    jsonRequestString = mapper.writeValueAsString(mListLayout);
                } catch (IOException e) {
//                   e.printStackTrace();
                }
                String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_LAYOUT_TIME_OFFLINE);
                sendSyncLayoutRequest(context, url, jsonRequestString, mListLayout);
            }
        } catch (Exception e) {

        }
    }


//    private void sendSyncRequest(final Context ctx, final String url, String arrayJson) {
//        JSONArray jsonRequest = null;
//
//        try {
//            jsonRequest = new JSONArray(arrayJson);
//        } catch (JSONException e) {
//        }
//        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, arrayJson, new Response.Listener<JSONArray>() {
//
//            @Override
//            public void onResponse(JSONArray response) {
//
//                Log.i("NetwrkRecr JSONObject ", response.toString());
//                if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_SCREEN_OFFLINE))) {
//
//                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_SCREEN_TABLE, onOffListScreen);
//
//                } else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_APP_OFFLINE))) {
//
//                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_APP_TABLE, onOffListAPP);
//
//                } else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_BOX_OFFLINE))) {
//
//                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_BOX_TABLE, onOffListBox);
//
//                } else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_LAYOUT_TIME_OFFLINE))) {
//
//                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeLayoutTimeData(mListLayout);
//
//                } else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_MEDIA_TIME_OFFLINE))) {
//
//                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeMediaTimeData(mList);
//                }
//                LogData.getInstance().setRequestSend(ctx, true);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                LogData.getInstance().setRequestSend(ctx, true);
//            }
//        });
//        VolleySingleton.getInstance(ctx).addToRequestQueue(request);
//    }

    private void sendSyncOnOffDataRequest(final Context ctx,final String url,String arrayJson,final Vector<OnOffTimeData> mlistOnOff){

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, arrayJson, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    Log.i("Volley Sync ", response.toString());
                    if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_SCREEN_OFFLINE))) {

                        DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_SCREEN_TABLE,mlistOnOff);

                    }else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_APP_OFFLINE))) {

                        DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_APP_TABLE,mlistOnOff);

                    }else if (url.equalsIgnoreCase(new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_BOX_OFFLINE))) {

                        DataCacheManager.getInstance(AppMain.getAppMainContext()).removeOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_BOX_TABLE,mlistOnOff);

                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

//                        Log.e("VolleyError ", error.getMessage());
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);

    }
    private void sendSyncLayoutRequest(final Context ctx,String url,String arrayJson,final Vector<LayoutTimeData> mlist){
        if(LogUtility.checkNetwork(ctx)){
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, arrayJson, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeLayoutTimeData(mlist);
                    try {
                        syncSavedLayoutData(ctx);
                    }catch (Exception e) {
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

//                        Log.e("VolleyError ", error.getMessage());
                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }
    }

    private void sendSyncMediaRequest(final Context ctx,String url,String arrayJson,final Vector<MediaTimeData> mlist) {
        if (LogUtility.checkNetwork(ctx)) {
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, arrayJson, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    DataCacheManager.getInstance(AppMain.getAppMainContext()).removeMediaTimeData(mlist);

                    try {
                        syncSavedMediaData(ctx);
                    } catch (Exception e) {
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
        }
    }
}
