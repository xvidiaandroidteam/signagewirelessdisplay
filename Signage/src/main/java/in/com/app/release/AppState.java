package in.com.app.release;

/**
 * This class has static fields for app.
 * @author Ravi@Xvidia
 * @since version 1.0
 *
 */
public class AppState {
	public static boolean ACTION_ON_PAUSE = true;
	public static boolean ACTION_CANCEL_TIMER = false;
	public static boolean BACKPRESSED_SIGNAGE_SCREEN = false;
	public static String wifiDisplayPackage = "com.rockchip.wfd";
	public static final String OLD_DISPLAY_FOLDER= "/Xvidia/Display/";
	public static final String FILE_FOLDER= "/Xvidia/";
	public static final String FILE_NAME_ASSET= "assetId.txt";
	public static final String FILE_NAME_BOX= "boxName.txt";
	public static final String FILE_NAME_HDMI= "hdmi.txt";
	public static final String FILE_NAME_Log= "log.txt";
	public static final String DISPLAY_FOLDER= "/Android/data/in.com.app.release/Xvidia/Display/";
	
	public static final String DOWNLOAD_FOLDER= "/Android/data/in.com.app.release/Xvidia/Download/";
}
