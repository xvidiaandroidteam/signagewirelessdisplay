package in.com.app.release.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryData extends DataParent {

	private String boxId = null;
	private String boxName = null;
	private String assetId = null;

	public String getBoxId() {
		return getValidatedString(boxId);
	}

	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}


	public String getBoxName() {
		return getValidatedString(boxName);
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public String getAssetId() {
		return getValidatedString(assetId);
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}


}
