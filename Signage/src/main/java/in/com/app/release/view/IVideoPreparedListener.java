package in.com.app.release.view;

public interface IVideoPreparedListener {

    void onVideoPrepared();
}