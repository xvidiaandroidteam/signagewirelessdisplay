package in.com.app.release;

import android.app.Application;
import android.content.Context;

import in.com.app.release.utility.MyExceptionHandler;

public class MyApplication extends Application{

    private static Context context;

    public void onCreate(){
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,AppMain.class));
        context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }
    
    public static void setAppContext(Context ctx) {
        context = ctx;
    }
   
}
