package in.com.app.release;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import in.com.app.release.BroadCastReceivers.AlarmReceiver;
import in.com.app.release.BroadCastReceivers.SyncLayoutReceiver;
import in.com.app.release.data.LogData;
import in.com.app.release.data.SignageData;
import in.com.app.release.model.IAPIConstants;
import in.com.app.release.model.InventoryData;
import in.com.app.release.model.LocationData;
import in.com.app.release.model.OnOffTimeData;
import in.com.app.release.network.ServiceURLManager;
import in.com.app.release.network.VolleySingleton;
import in.com.app.release.storage.caching.sqlight.MySqliteHelper;
import in.com.app.release.storage.caching.sqlight.manager.DataCacheManager;
import in.com.app.release.utility.LogUtility;
import in.com.app.release.utility.MyExceptionHandler;
import in.com.app.release.utility.NetworkUtil;
import in.com.app.release.utility.Utility;
import in.com.app.release.wifiDirect.ChatManager;
import in.com.app.release.wifiDirect.ClientSocketHandler;
import in.com.app.release.wifiDirect.GroupOwnerSocketHandler;
import in.com.app.release.wifiDirect.WiFiChatFragment;
import in.com.app.release.wifiDirect.WiFiDirectBroadcastReceiver;
import in.com.app.release.wifiDirect.WiFiDirectServicesList;
import in.com.app.release.wifiDirect.WiFiP2pService;

/**
 * 
 * @author Ravi@Xvidia Technologies
 * @version 1.0
 * This is the Main class where registration is processed  and layout is downloaded.
 * IpAddress, MacId, Location, Network Type, Network Status information is fetched.
 *
 */
public class AppMain extends Activity implements WiFiDirectServicesList.DeviceClickListener,
		IDisplayLayout, Handler.Callback, WifiP2pManager.ConnectionInfoListener {


	private final int _TIME_TO_REHIT_SERVER =15*1000;
	private final int MIN_TIME_LOCATION_UPDATES = 5*1000;
    private int layoutWidth =0;
    private int layoutHeight = 0;
//    private static Handler UIHandler;
    static ProgressBar pb;
    static TextView dialogtext ;
    static Dialog dialog;
    static TextView cur_val;
	private static Handler handler = null;
	static private Runnable runnableBackground;
	public final String TAG = AppMain.class.getCanonicalName();
	private TextView textViewRegID = null;
	private TextView textViewMacId = null;
	private TextView textViewIpAddress = null;
	private TextView textViewLocation = null;
	private TextView textViewAppVersion= null;
	private TextView textViewResolution = null;
	public static TextView textViewInfo = null;
	public static TextView connectionStatus = null;
	private LinearLayout locationlayout;
	private PopupWindow pwindo;
	private Button /*btnCancel, btnSubmit,*/screenCastToggleButton, btnSave;
	private EditText userName;
	private EditText password;
	private LocationManager mLocationManager;
	private MyLocationListener mylistener;
	private static Runnable refreshRunnable;
	private static Handler refreshHandler;
//	private boolean cancelPopup = false;
//	final int toastTime = 3000;
//	final int HEARTBEAT_TIME = 300*1000;
	private final int TIMER_TIME = 30000;
	private String android_id;
	private EditText editBoxDisplayName;
	private EditText editBoxAddress;
	private EditText editBoxAssetId;
	private int currentApiVersion;
    private Spinner spinnerOrientation;
	/** WifiP2pManager */
	private WifiP2pManager mWifiP2pManager;
	/** Channel */
	private WifiP2pManager.Channel mChannel;

	// TXT RECORD properties
	public static final String TXTRECORD_PROP_AVAILABLE = "available";
	public static final String SERVICE_INSTANCE = "_wifidemotest";
	public static final String SERVICE_REG_TYPE = "_presence._tcp";

	public static final int MESSAGE_READ = 0x400 + 1;
	public static final int MY_HANDLE = 0x400 + 2;
	private WifiP2pManager manager;

	static final int SERVER_PORT = 4545;

	private final IntentFilter intentFilter = new IntentFilter();
	private WifiP2pManager.Channel channel;
	private BroadcastReceiver receiver = null;
	private WifiP2pDnsSdServiceRequest serviceRequest;

	private Handler handler2 = new Handler(this);
	private WiFiChatFragment chatFragment;
	private ChatManager chatManager;
	private WiFiDirectServicesList servicesList;

	private TextView statusTxtView;

	public Handler getHandler() {
		return handler2;
	}

	public void setHandler(Handler handler) {
		this.handler2 = handler;
	}

	@TargetApi(19)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		  currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		    // This work only for android 4.4+
		    if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
		    {

		        getWindow().getDecorView().setSystemUiVisibility(flags);

		        // Code below is to handle presses of Volume up or Volume down.
		        // Without this, after pressing volume buttons, the navigation bar will
		        // show up and won't hide
		        final View decorView = getWindow().getDecorView();
		        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
		            {

		                @Override
		                public void onSystemUiVisibilityChange(int visibility)
		                {
		                    if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
		                    {
		                        decorView.setSystemUiVisibility(flags);
		                    }
		                }
		            });
		    }
		setContentView(R.layout.layout_appmain);
		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,AppMain.class));
		context = getApplicationContext();
        showProgress();
		startUpdater();
//        UIHandler = new Handler(Looper.getMainLooper());
		AppState.ACTION_ON_PAUSE= false;
		AppState.ACTION_CANCEL_TIMER = false;
		android_id  = ""+System.currentTimeMillis();
		Random r = new Random();
		int temp = r.nextInt(999999999) + 65;
		android_id = android_id+temp;
		LogData.getInstance().setRequestSend(context, true);
		ClientConnectionConfig._DISPLAYNAME =  LogData.getInstance().getDisplayName(getApplicationContext());
		ClientConnectionConfig._HARDWAREKEY =LogData.getInstance().getAppID(getApplicationContext());//(_KEY_XVIDIA_HARDWARE_KEY);
		ClientConnectionConfig._ADDRESS= LogData.getInstance().getAddress(getApplicationContext());
		ClientConnectionConfig._ASSETID =FileManager.readTextFromFile(AppState.FILE_NAME_ASSET);
		
		if(ClientConnectionConfig._ASSETID.isEmpty() || ClientConnectionConfig._ASSETID.equalsIgnoreCase(LogData.STR_UNKNOWN)){
			ClientConnectionConfig._ASSETID = "";
		}
		if(ClientConnectionConfig._DISPLAYNAME.isEmpty()|| ClientConnectionConfig._DISPLAYNAME.equalsIgnoreCase(LogData.STR_UNKNOWN)){
			String appIdStr = DataCacheManager.getInstance(getAppMainContext()).readSettingData(_KEY_XVIDIA_DISPLAY_NAME);
//			String tempStr = appIdStr;		
			if(!appIdStr.isEmpty()){
				LogData.getInstance().setDisplayname(getAppMainContext(), appIdStr);
				DataCacheManager.getInstance(getAppMainContext()).saveSettingData(_KEY_XVIDIA_DISPLAY_NAME,"");
			}else if(!ClientConnectionConfig._ASSETID.isEmpty()){
				appIdStr = ClientConnectionConfig._ASSETID;
				LogData.getInstance().setDisplayname(getAppMainContext(), appIdStr);
			}
			ClientConnectionConfig._DISPLAYNAME = appIdStr;
			
		}
		if(ClientConnectionConfig._HARDWAREKEY.isEmpty()|| ClientConnectionConfig._HARDWAREKEY.equalsIgnoreCase(LogData.STR_UNKNOWN)){
			String appIdStr = DataCacheManager.getInstance(getAppMainContext()).readSettingData(_KEY_XVIDIA_HARDWARE_KEY);
			if(!appIdStr.isEmpty()){
				LogData.getInstance().setAppID(getAppMainContext(), appIdStr);
				DataCacheManager.getInstance(getAppMainContext()).saveSettingData(_KEY_XVIDIA_HARDWARE_KEY,"");
			}
			ClientConnectionConfig._HARDWAREKEY = appIdStr;
			
		}
		if(ClientConnectionConfig._ADDRESS.isEmpty()|| ClientConnectionConfig._ADDRESS.equalsIgnoreCase(LogData.STR_UNKNOWN)){

			ClientConnectionConfig._ADDRESS ="";
		}
        locationlayout= (LinearLayout)findViewById(R.id.layout_Location);
        spinnerOrientation = (Spinner)findViewById(R.id.spinner_orientation);
        String[] orientation = getResources().getStringArray(R.array.menu_orientation);
                ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, orientation);
        	  adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        	  spinnerOrientation.setAdapter(adapter_state);
        	  spinnerOrientation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position,
                                             long id) {
                      spinnerOrientation.setSelection(position);
//                      String selState = (String) spinnerOrientation.getSelectedItem();
                      if (position == 0) {
                          LogData.getInstance().setOrientation(getAppMainContext(), ORIENTATION_LANSCAPE);
                      } else if (position == 1) {
                          LogData.getInstance().setOrientation(getAppMainContext(), ORIENTATION_REVERSE_LANSCAPE);
                      } else if (position == 2) {
                          LogData.getInstance().setOrientation(getAppMainContext(), ORIENTATION_PORTRAIT);
                      } else if (position == 3) {
                          LogData.getInstance().setOrientation(getAppMainContext(), ORIENTATION_REVERSE_PORTRAIT);
                      }
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> arg0) {

                  }
              });
		
		if(LogData.getInstance().getOrientation(getAppMainContext())== ORIENTATION_LANSCAPE){
//			landscape.setChecked(true);
            spinnerOrientation.setSelection(0);
		}else if(LogData.getInstance().getOrientation(getAppMainContext())== ORIENTATION_REVERSE_LANSCAPE){
//			landscapeReverse.setChecked(true);
          spinnerOrientation.setSelection(1);
		}else if(LogData.getInstance().getOrientation(getAppMainContext())== ORIENTATION_PORTRAIT){
//			portrait.setChecked(true);
            spinnerOrientation.setSelection(2);
		}else if(LogData.getInstance().getOrientation(getAppMainContext())== ORIENTATION_REVERSE_PORTRAIT){
//			portraitReverse.setChecked(true);
            spinnerOrientation.setSelection(3);
		}
		editBoxDisplayName = (EditText) findViewById(R.id.id_txtDisplayName);
		editBoxDisplayName.setText(ClientConnectionConfig._DISPLAYNAME);
		editBoxDisplayName.setOnEditorActionListener(new OnEditorActionListener() {
			//initialiseInternalCamera();
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
		        case EditorInfo.IME_ACTION_DONE:
					FileManager.writeTextToFile(editBoxDisplayName.getText().toString(),AppState.FILE_NAME_BOX);
		        	hideSoftKeyboard(editBoxDisplayName);
		            break;
				case EditorInfo.IME_ACTION_NEXT:
					FileManager.writeTextToFile(editBoxDisplayName.getText().toString(),AppState.FILE_NAME_BOX);
					break;
		        }
		        return false;
			}

			
		});

		editBoxDisplayName.clearFocus();
		editBoxAddress = (EditText) findViewById(R.id.id_txtAddress);
		editBoxAddress.setText(ClientConnectionConfig._ADDRESS);
		editBoxAddress.setOnEditorActionListener(new OnEditorActionListener() {
			//initialiseInternalCamera();
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
		        case EditorInfo.IME_ACTION_DONE:
		        	hideSoftKeyboard(editBoxAddress);
		            break;
		        case EditorInfo.IME_ACTION_NEXT:
		        	if(!ClientConnectionConfig._ASSETID.isEmpty()){
		        		hideSoftKeyboard(editBoxAddress);
		        	}
		            break;
		        }
		        return false;
			}

			
		});
		editBoxAddress.clearFocus();
		editBoxAssetId = (EditText) findViewById(R.id.id_txtAssetId);
		btnSave = (Button) findViewById(R.id.btnSaveAsset);
		editBoxAssetId.setText(ClientConnectionConfig._ASSETID);
		if(!ClientConnectionConfig._ASSETID.isEmpty()){
//			editBoxAssetId.setOnClickListener(this);
//		}else{
			editBoxAssetId.setKeyListener(null);
			btnSave.setVisibility(View.GONE);
		}
		editBoxAssetId.setOnEditorActionListener(new OnEditorActionListener() {
			//initialiseInternalCamera();
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
		        case EditorInfo.IME_ACTION_DONE:
		        	if(!editBoxAssetId.getText().toString().isEmpty()){
		        		FileManager.writeTextToFile(editBoxAssetId.getText().toString(),AppState.FILE_NAME_ASSET);
//		        		DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_ASSETID, editBoxAssetId.getText().toString());
		    			LogData.getInstance().setAssetID(getApplicationContext(), editBoxAssetId.getText().toString());
		        	}else{
		        		Toast tStatus = Toast.makeText(AppMain.this, "Please enter asset ID",Toast.LENGTH_SHORT);
		    			tStatus.show();
		        	}
		        	hideSoftKeyboard(editBoxAssetId);
		            break;
		        case EditorInfo.IME_ACTION_NEXT:
		        	if(!editBoxAssetId.getText().toString().isEmpty()){
		        		LogData.getInstance().setAssetID(getApplicationContext(), editBoxAssetId.getText().toString());
		        		FileManager.writeTextToFile(editBoxAssetId.getText().toString(),AppState.FILE_NAME_ASSET);
		        	}
		        	hideSoftKeyboard(editBoxAssetId);
		            break;
		        }
		        return false;
			}

			
		});
		editBoxAssetId.clearFocus();
		btnSave.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				if(!editBoxAssetId.getText().toString().isEmpty()){
					ClientConnectionConfig._ASSETID = editBoxAssetId.getText().toString();
					LogData.getInstance().setAssetID(getAppMainContext(), ClientConnectionConfig._ASSETID);
	        		FileManager.writeTextToFile(ClientConnectionConfig._ASSETID,AppState.FILE_NAME_ASSET);
//	        		DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_ASSETID, ClientConnectionConfig._ASSETID);

	    			editBoxAssetId.setKeyListener(null);
	    			btnSave.setVisibility(View.GONE);
	        	}else{
	        		Toast tStatus = Toast.makeText(AppMain.this, "Please enter asset ID", Toast.LENGTH_SHORT);
	    			tStatus.show();
	        	}
			}});
//		updateNewFolderStructure();
		Button btnRegister = (Button) findViewById(R.id.btnRegister);
		Button btnRefresh = (Button) findViewById(R.id.btnRefresh);
		Button btnRoot = (Button) findViewById(R.id.btnRoot);
		Button btnWifiSetting = (Button) findViewById(R.id.btnWifiSetting);
		screenCastToggleButton = (Button)findViewById(R.id.toggle_screen_cast);
		screenCastToggleButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!LogData.getInstance().getScreenCastEnabled(getAppMainContext())) {
					LogData.getInstance().setScreenCastEnabled(getAppMainContext(), true);
					if (!isAppRunningInBackground()) {
						launchWfdApplication();
					}
					screenCastToggleButton.setText(getString(R.string.button_screen_cast_enabled));
					LogData.getInstance().setScreenCastEnabled(getAppMainContext(), true);
					startActivity(new Intent(AppMain.this, ScreenMirrorActivity.class));
				} else {
					LogData.getInstance().setScreenCastEnabled(getAppMainContext(), false);
					screenCastToggleButton.setText(getString(R.string.button_screen_cast_disabled));
				}
			}
		});


		textViewRegID = (TextView) findViewById(R.id.id_txtRegistrationId);
		textViewMacId = (TextView) findViewById(R.id.id_txtMacId);
		textViewIpAddress = (TextView) findViewById(R.id.id_txtIpId);
		textViewLocation = (TextView) findViewById(R.id.id_txtLocation);
		textViewResolution = (TextView) findViewById(R.id.id_txtResolution);
		textViewInfo = (TextView) findViewById(R.id.id_txtInfo);
		textViewAppVersion = (TextView) findViewById(R.id.id_version);
		connectionStatus = (TextView) findViewById(R.id.id_txtConnection);
		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(5000); //You can manage the blinking time with this parameter
		anim.setStartOffset(20);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		textViewInfo.startAnimation(anim);
		textViewAppVersion.setText(getAppVersion());
		btnRoot.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				installNewApk();

			}});
		btnRegister.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				//REMOVE ALL MEDIA DATA///
				registerDevice();
//				changeSystemTime("2015","04","06","13","09","30");
			}});
		btnRefresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				DataCacheManager.getInstance(getAppMainContext()).removeResourceDataAll();
				LogData.getInstance().setCurrentLayoutXml(getAppMainContext(), LogData.STR_UNKNOWN);
				SignageData.resetSignageData();
				DataCacheManager.getInstance(getAppMainContext()).clearSettingCache();
				registerDevice();
//				installNewApk();
			}
		});

		btnWifiSetting.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				openWifiSetting();
			}
		});

		if(LogData.getInstance().getScreenCastEnabled(context)){
			screenCastToggleButton.setText(getString(R.string.button_screen_cast_enabled));
			initialiseWifip2pManager();

		}else{
			screenCastToggleButton.setText(getString(R.string.button_screen_cast_disabled));

		}
		/**
		 * 
		 *Timer is started to if returned from display screen 
		 *to reinvoke display if screen is idle for 180 seconds
		 *
		**/
		if (AppState.BACKPRESSED_SIGNAGE_SCREEN) {
			btnRegister.setText(getResources().getString(R.string.btnStart));
//			startTimer(TIMER_TIME);
		}

		refreshScreen();
		new InternetAccessible().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
		textViewInfo.setVisibility(View.GONE);
		/**
		 * 
		 *It Switches to display layout in offline mode 
		 *if STB is already registered and layout previously downloaded.
		 *
		**/
		if( ClientConnectionConfig._HARDWAREKEY.equals("")||ClientConnectionConfig._HARDWAREKEY.equalsIgnoreCase(LogData.STR_UNKNOWN) ){
			try{
				android_id = Secure.getString(context.getContentResolver(),
						Secure.ANDROID_ID);
			}
			catch (Exception e) {

			}
			setInitialValue(android_id);
			ClientConnectionConfig._HARDWAREKEY = android_id;
			registerDevice();
		}else{
			android_id = ClientConnectionConfig._HARDWAREKEY;
			setInitialValue(android_id);
			editBoxDisplayName.setText(ClientConnectionConfig._DISPLAYNAME);
			getBoxNameAndAssetIdData(getAppMainContext());
//			if(checkOfflineTimedOut(30)){
//				textViewInfo.setText(R.string.register_complete);
//				textViewInfo.setTextColor(getResources().getColor(R.color.red_color));
//				textViewInfo.setVisibility(View.VISIBLE);
////				progressBar.setVisibility(View.GONE);
//			}else
 			if(!AppState.BACKPRESSED_SIGNAGE_SCREEN){
//				cancelPopup = true;
				String xml = LogData.getInstance().getCurrentLayoutXml(getAppMainContext());
				
				if(xml!=null && !xml.equalsIgnoreCase("") &&!xml.equalsIgnoreCase(LogData.STR_UNKNOWN)){
					StateMachine.displayingContentInOffileMode = false;
					btnRegister.setText(getResources().getString(R.string.btnStart));
					StateMachine.gi(AppMain.this).initProcess(false, StateMachine.OFFLINE);
				}else{
					String username = editBoxDisplayName.getText().toString();
		        	if(username.equals("")|| username.length()==0){
		        		editBoxDisplayName.requestFocus();
					}else{
			        	hideSoftKeyboard(editBoxDisplayName);
					}
		        	btnRegister.setText(getResources().getString(R.string.btnStart));
//					Display display = getWindowManager().getDefaultDisplay();
//					Point size = new Point();
//					if(currentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR1){
//						display.getRealSize(size);
//					}else{
//						display.getSize(size);
//					}
//					layoutWidth = size.x;
//					layoutHeight = size.y;
//					String resolution = ""+layoutWidth+"X"+layoutHeight;
					LogData.getInstance().setDisplayname(getAppMainContext(), ClientConnectionConfig._DISPLAYNAME);
					LogData.getInstance().setAppID(getAppMainContext(), ClientConnectionConfig._HARDWAREKEY);
					LogData.getInstance().setAddress(getAppMainContext(), ClientConnectionConfig._ADDRESS);
					LogData.getInstance().setAssetID(getAppMainContext(), ClientConnectionConfig._ASSETID);
					StateMachine.gi(AppMain.this).initProcess(true, StateMachine.REGISTER);
					editBoxDisplayName.setText(ClientConnectionConfig._DISPLAYNAME);
				}
//			}else{
//				registerDevice();
			}
//			return;
		}

		cancelLayoutAlarm(context);
		enableWifi();

		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		channel = manager.initialize(this, getMainLooper(), null);
		startRegistrationAndDiscovery();

		servicesList = new WiFiDirectServicesList();
		getFragmentManager().beginTransaction()
				.add(R.id.container_root, servicesList, "services").commit();

	}

	/**
	 * Registers a local service and then initiates a service discovery
	 */
	private void startRegistrationAndDiscovery() {
		Map<String, String> record = new HashMap<String, String>();
		record.put(TXTRECORD_PROP_AVAILABLE, "visible");

		WifiP2pDnsSdServiceInfo service = null;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			service = WifiP2pDnsSdServiceInfo.newInstance(
					SERVICE_INSTANCE, SERVICE_REG_TYPE, record);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			manager.addLocalService(channel, service, new WifiP2pManager.ActionListener() {

				@Override
				public void onSuccess() {
					Log.e(TAG,"Added Local Service");
//					appendStatus("Added Local Service");
				}

				@Override
				public void onFailure(int error) {
					Log.e(TAG,"Failed to add a service");
//					appendStatus("Failed to add a service");
				}
			});
		}

		discoverService();

	}

	private void discoverService() {

        /*
         * Register listeners for DNS-SD services. These are callbacks invoked
         * by the system when a service is actually discovered.
         */

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			manager.setDnsSdResponseListeners(channel,
					new WifiP2pManager.DnsSdServiceResponseListener() {

						@Override
						public void onDnsSdServiceAvailable(String instanceName,
															String registrationType, WifiP2pDevice srcDevice) {

							// A service has been discovered. Is this our app?

							if (instanceName.equalsIgnoreCase(SERVICE_INSTANCE)) {

								// update the UI and add the item the discovered
								// device.
								WiFiDirectServicesList fragment = (WiFiDirectServicesList) getFragmentManager()
										.findFragmentByTag("services");
								if (fragment != null) {
									WiFiDirectServicesList.WiFiDevicesAdapter adapter = ((WiFiDirectServicesList.WiFiDevicesAdapter) fragment
											.getListAdapter());
									WiFiP2pService service = new WiFiP2pService();
									service.device = srcDevice;
									service.instanceName = instanceName;
									service.serviceRegistrationType = registrationType;
									adapter.add(service);
									adapter.notifyDataSetChanged();
									Log.d(TAG, "onBonjourServiceAvailable "
											+ instanceName);
								}
							}

						}
					}, new WifiP2pManager.DnsSdTxtRecordListener() {

						/**
						 * A new TXT record is available. Pick up the advertised
						 * buddy name.
						 */
						@Override
						public void onDnsSdTxtRecordAvailable(
								String fullDomainName, Map<String, String> record,
								WifiP2pDevice device) {
							Log.d(TAG,
									device.deviceName + " is "
											+ record.get(TXTRECORD_PROP_AVAILABLE));
						}
					});
		}

		// After attaching listeners, create a service request and initiate
		// discovery.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			manager.addServiceRequest(channel, serviceRequest,
					new WifiP2pManager.ActionListener() {

						@Override
						public void onSuccess() {
							Log.e(TAG,"Added service discovery request");
						}

						@Override
						public void onFailure(int arg0) {
							Log.e(TAG,"Failed adding service discovery request");
//							appendStatus("Failed adding service discovery request");
						}
					});
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			manager.discoverServices(channel, new WifiP2pManager.ActionListener() {

				@Override
				public void onSuccess() {
					Log.e(TAG,"Service discovery initiated");
//					appendStatus("Service discovery initiated");
				}

				@Override
				public void onFailure(int arg0) {
					Log.e(TAG,"Service discovery failed");
//					appendStatus("Service discovery failed");

				}
			});
		}
	}

	@Override
	public void connectP2p(WiFiP2pService service) {
		WifiP2pConfig config = new WifiP2pConfig();
		config.deviceAddress = service.device.deviceAddress;
		config.wps.setup = WpsInfo.PBC;
		if (serviceRequest != null)
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				manager.removeServiceRequest(channel, serviceRequest,
						new WifiP2pManager.ActionListener() {

							@Override
							public void onSuccess() {
							}

							@Override
							public void onFailure(int arg0) {
							}
						});
			}

		manager.connect(channel, config, new WifiP2pManager.ActionListener() {

			@Override
			public void onSuccess() {
				Log.e(TAG,"Connecting to service");
//				appendStatus("Connecting to service");
			}

			@Override
			public void onFailure(int errorCode) {
				Log.e(TAG,"Failed connecting to service");
//				appendStatus("Failed connecting to service");
			}
		});
	}


	private void setSyncLayoutAlarm(Context context) {
		Calendar updateTime = Calendar.getInstance();
		Intent downloader = new Intent(context, SyncLayoutReceiver.class);
		PendingIntent recurringDownload = PendingIntent.getBroadcast(context,
				0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				updateTime.getTimeInMillis(),
				2500, recurringDownload);
//		alarms.setExact(AlarmManager.RTC_WAKEUP,updateTime.getTimeInMillis(), recurringDownload);

	}

	public void setDeviceName(String devName) {
		try {
			Class[] paramTypes = new Class[3];
			paramTypes[0] = WifiP2pManager.Channel.class;
			paramTypes[1] = String.class;
			paramTypes[2] = WifiP2pManager.ActionListener.class;
			Method setDeviceName = mWifiP2pManager.getClass().getMethod(
					"setDeviceName", paramTypes);
			setDeviceName.setAccessible(true);

			Object arglist[] = new Object[3];
			arglist[0] = mChannel;
			arglist[1] = devName;
			arglist[2] = new WifiP2pManager.ActionListener() {

				@Override
				public void onSuccess() {
//					Toast.makeText(getAppMainContext(),"setDeviceName succeeded ",Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onFailure(int reason) {
//					Toast.makeText(getAppMainContext(), "setDeviceName failed",Toast.LENGTH_SHORT).show();
				}
			};

			setDeviceName.invoke(mWifiP2pManager, arglist);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void enableWifi() {
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if (wifiManager == null)
			return;
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
	}

	private void initialiseWifip2pManager(){
		try {
			enableWifi();

			PackageManager pm = getBaseContext().getPackageManager();
			boolean flag = pm.hasSystemFeature(PackageManager.FEATURE_WIFI_DIRECT);
			if (flag) {
				mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
//        if (isNull(false)) { return; }

				mChannel = mWifiP2pManager.initialize(this, getMainLooper(), new WifiP2pManager.ChannelListener() {
					public void onChannelDisconnected() {
					}
				});
				if (mWifiP2pManager != null && mChannel != null) {
					discoverPeers();
					String name = ClientConnectionConfig._HARDWAREKEY;
					if(name.length() >4){
						int length = name.length();
						name = name.substring(length-3);
					}
					name = "Xonecast_"+name;
					setDeviceName(name);
				}
			}
		}catch(Exception e){

		}
	}

	private void discoverPeers(){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mWifiP2pManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
					@Override
					public void onSuccess() {

//						Toast.makeText(getAppMainContext(),"Successs ",Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onFailure(int reason) {
						Toast.makeText(getAppMainContext(),"discover failed ",Toast.LENGTH_SHORT).show();
					}
				});}
		},1500);
	}

	private void openWifiSetting(){
		String pac = "com.android.settings";
		Intent i = new Intent();

		i.setClassName(pac, pac + ".wifi.p2p.WifiP2pSettings");
		try {
			startActivity(i);
		} catch (ActivityNotFoundException e) {
			if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH+1) {
				startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
			} else {
				i.setClassName(pac, pac + ".wifi.WifiSettings"); 	try {
					startActivity(i);
				} catch (ActivityNotFoundException e2) {
				}
			}
		}
	}

	private boolean isAppRunningInBackground() {
		boolean retValue = false;
		final ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);

		List<ActivityManager.RunningAppProcessInfo> mTasks = am.getRunningAppProcesses();

		for (int i = 0; i < mTasks.size(); i++) {
			String name = mTasks.get(i).processName;

			if (name.equals(AppState.wifiDisplayPackage)) {
				retValue = true;
				break;
			}
		}
		return retValue;
	}
	/**
	 * Launch Another application
	 */
	void launchWfdApplication() {
		try {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {

					setSyncLayoutAlarm(MyApplication.getAppContext());
				}
			}, 1000);
			Intent intent = getPackageManager().getLaunchIntentForPackage(AppState.wifiDisplayPackage);
			if (intent != null) {
				Toast.makeText(AppMain.this, "WFD LAUNCH", Toast.LENGTH_SHORT).show();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		} catch (ActivityNotFoundException e) {
		} catch (Exception e) {
		}
	}

	private void cancelLayoutAlarm(Context context) {
		try{
			Intent downloader = new Intent(context, SyncLayoutReceiver.class);
			PendingIntent recurringDownload = PendingIntent.getBroadcast(context,
					0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
			AlarmManager alarms = (AlarmManager) getSystemService(
					Context.ALARM_SERVICE);
			alarms.cancel(recurringDownload);
		}catch(Exception e){
			e.printStackTrace();
		}

	}
    /**
     * Checks if the application is running in offline mode for more than a stipulated time
     * @param days
     * @return
     */
	private boolean checkOfflineTimedOut(int days){
		boolean returnValue = false;
		if(LogUtility.checkNetwork(getAppMainContext())){
			LogData.getInstance().setOfflineSinceTimeStamp(getAppMainContext(),0);
			return false;
		}else {
			long timeInMilliseconds = LogData.getInstance().getOfflineSinceTimeStamp(AppMain.getAppMainContext());
			if (timeInMilliseconds > 0) {
				long currentTime = System.currentTimeMillis();
				long diffInMilliSecs = currentTime - timeInMilliseconds;
				int diffInDays = (int) (diffInMilliSecs / (1000 * 60 * 60 * 24));
				if (diffInDays >= days) {
					returnValue = true;
				}
			}
		}
		return returnValue;

	}



	private void changeSystemTime(String year,String month,String day,String hour,String minute,String second){
		try {
			Process process = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(process.getOutputStream());
			String command = "date -s "+year+month+day+"."+hour+minute+second+"\n";
			Log.e("command",command);
			os.writeBytes(command);
			os.flush();
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    /**
     * Returns application version
     * @return
     */
	private String getAppVersion(){
		String version = "1.0";
		try {
		PackageManager manager = context.getPackageManager();
		PackageInfo info;
			info = manager.getPackageInfo(
			    context.getPackageName(), 0);
			version = info.versionName;
			String savedVersion = LogData.getInstance().getAppVersion(getAppMainContext());
			if(!savedVersion.equalsIgnoreCase("")) {
				Double savedVal = Utility.convertToDoubleFromString(savedVersion);
				Double verClient = Utility.convertToDoubleFromString(version);
				if (verClient < savedVal) {
					version = savedVal.toString();
				}
			}
		} catch (NameNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		}catch(Exception e){

        }
		return version;
	}

	/**
	 * This function sends the Box data request to server
	 * @param ctx
	 */
	private void getBoxNameAndAssetIdData(Context ctx){
		try {

			String url =new ServiceURLManager().getUrl(IAPIConstants.API_KEY_BOX_GET_INVENTORY_DATA);
			url = url+ClientConnectionConfig._HARDWAREKEY;
			JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					ObjectMapper mapper = new ObjectMapper();
					InventoryData obj = null;
					try {
						obj = mapper.readValue(response.toString(), InventoryData.class);
					} catch (IOException e) {
						e.printStackTrace();
					}
					String deviceId = obj != null ? obj.getBoxId() : "";
					String boxName = obj != null ? obj.getBoxName() : "";
					String assetId =obj != null ? obj.getAssetId() : "";
					if (!deviceId.isEmpty() && deviceId.equals(ClientConnectionConfig._HARDWAREKEY)
							&& !assetId.isEmpty() && assetId.equals(ClientConnectionConfig._ASSETID)&& !boxName.isEmpty()) {
						LogData.getInstance().setDisplayname(getAppMainContext(), boxName);
						ClientConnectionConfig._DISPLAYNAME = boxName;
						editBoxDisplayName.setText(boxName);
					}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

//                        Log.e("VolleyError ", error.getMessage());
				}
			});
			VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
		}catch(Exception e){

		}
	}

    /**
     * This function sends the log request to server
     * @param ctx
     * @param locationdata
     */
	private void sendLocationRequest(Context ctx, LocationData locationdata){
		  try {
			  if(LogUtility.checkNetwork(ctx)){
				  ObjectMapper mapper = new ObjectMapper();
				  String jsonRequestString = null;
				  try {
					  jsonRequestString = mapper.writeValueAsString(locationdata);
				  } catch (IOException e) {
					  e.printStackTrace();
				  }
				  JSONObject jsonRequest = null;
				  try {
					  jsonRequest = new JSONObject(jsonRequestString);
				  } catch (JSONException e) {
//                          e.printStackTrace();
				  }
				  final String url =new ServiceURLManager().getUrl(IAPIConstants.API_KEY_BOX_LOCATION);
				  JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

					  @Override
					  public void onResponse(JSONObject response) {
						  ObjectMapper mapper = new ObjectMapper();
						  LocationData obj = null;
						  try {
							  obj = mapper.readValue(response.toString(), LocationData.class);
						  } catch (IOException e) {
							  e.printStackTrace();
						  }
						  String deviceId = obj != null ? obj.getId() : "";
						  if (!deviceId.isEmpty()) {
							 LogData.getInstance().setLocation(getAppMainContext(),LogData.STR_UNKNOWN);
						  }
					  }
				  }, new Response.ErrorListener() {

					  @Override
					  public void onErrorResponse(VolleyError error) {

//                        Log.e("VolleyError ", error.getMessage());
					  }
				  });
				  VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			  }
			}catch(Exception e){
				
			}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
	    super.onWindowFocusChanged(hasFocus);
	    if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
	    {
	        getWindow().getDecorView().setSystemUiVisibility(
	            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
	                | View.SYSTEM_UI_FLAG_FULLSCREEN
	                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	    }
	}
//	private void setClientVersion(){
//		try{
////			boolean logFlag = LogData.getInstance().setAppVersion(getApplicationContext(), clientVersion); //
////			boolean logFlag = LogData.getInstance().setAppStatus(getApplicationContext(),LogData.APP_ON);
//			if(logFlag){
////				sendLogRequest(getApplicationContext(), LogUtility.getInstance().getAppVersionJson(getApplicationContext()), LogUtility.getAppVersionLogUrl());
//			  }
////			DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_CLIENT_VERSION, clientVersion);
//		}catch(Exception e){
////			Toast.makeText(this, " setClientVersion "+e.getMessage(), Toast.LENGTH_LONG).show();
//		}
//	}
	private void registerDevice(){
//		String username = editBoxDisplayName.getText().toString();
//    	if(username != null && username.equals("")|| username.length()==0){	        		
//    		editBoxDisplayName.requestFocus();
//		}else{
        	hideSoftKeyboard(editBoxDisplayName);
//		}
//		setClientVersion();
//		DataCacheManager.getInstance().removeResourceDataAll();
//		SignageData.resetSignageData();
		saveData();
	}

	/**
	 * This method installs and restarts apk only on rooted STB
	 */
	private void installNewApk()
	{
		String path = Environment.getExternalStorageDirectory()+ "/Download/dummy.apk";
		final String libs = "LD_LIBRARY_PATH=/vendor/lib:/system/lib ";
		final String[] commands = {
				libs + "pm install -r " + path,
				libs + "am start -n " + AppMain.getAppMainContext().getPackageName() + "/" + get_main_activity()
		};
        try
        {
//	            Runtime.getRuntime().exec(new String[] {"su", "-c", "pm install -r "+path});
//	            Toast.makeText(AppMain.this, "installing ", toastTime).show();
          // Do the magic
    			Process p = Runtime.getRuntime().exec( "su" );
    			InputStream es = p.getErrorStream();
    			DataOutputStream os = new DataOutputStream(p.getOutputStream());

    			for( String command : commands ) {
    				//Log.i(TAG,command);
    				os.writeBytes(command 	+ "\n");
    			}
    			os.writeBytes("exit\n");
    			os.flush();

    			int read;
    			byte[] buffer = new byte[4096];
    			String output = "";
    			while ((read = es.read(buffer)) > 0) {
    			    output += new String(buffer, 0, read);
    			}

    			p.waitFor();

//    			Toast.makeText(this, output.trim() + " (" + p.exitValue() + ")", Toast.LENGTH_LONG).show();
    		} catch (IOException e) {
    			
    		} catch (InterruptedException e) {
    			
    		}
	}
	
	/**
	 * This method returns the package info of current project
	 * @return package name
	 */
	private String get_main_activity() {
		PackageManager pm = AppMain.getAppMainContext().getPackageManager();
		String packageName = AppMain.getAppMainContext().getPackageName();

		try {
			final int flags = PackageManager.GET_ACTIVITIES;
			PackageInfo packageInfo = pm.getPackageInfo(packageName, flags);
			for( ActivityInfo ai : packageInfo.activities ) {
				if( ai.exported ) {
					return ai.name;
				}
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		Toast.makeText(this, "get_main_activity() failed", Toast.LENGTH_LONG).show();
		return "";
	}
	/**
	 * In this method date string is formatted in particular date format
	 * @param dateString simple date String to be formatted
	 * @return the dateString formated in "yyyy-MM-dd HH:mm:ss.SSS"
	 * @deprecated
	 * @since version 1.0
	 */
//	String getMillseconds(String dateString){
//
//		if (dateString != null) {
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
//			Date testDate = null;
//			try {
//				testDate = sdf.parse(dateString);
//				long millisec = testDate.getTime();
//				dateString = ""+millisec;
//			} catch (ParseException e) {
//			}
//
//
//		}
//		return dateString;
//	}

	/**
	 * This function hides the soft Keyboard
	 * @param editbox editbox -reference to editext for which soft keyboard is to be hidden
	 * @since version 1.0
	 */
	private void hideSoftKeyboard(EditText editbox){
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editbox.getWindowToken(), 0);
	}
	
	/**
	 * In this method Location services is initialised
	 */
	private void initializeLocationManager(){
		// Get the location manager
//		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//		  // Define the criteria how to select the location provider
		if ( Build.VERSION.SDK_INT >= 23 &&
				ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
				ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return  ;
		}
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);	//default

		criteria.setCostAllowed(false);
//		  // get the best provider depending on the criteria
//
//		  // the last known location of this provider
//		  Location location = mLocationManager.getLastKnownLocation(provider);
//
//		  mylistener = new MyLocationListener();
//
//		  if (location != null) {
//			  mylistener.onLocationChanged(location);
//			  mLocationManager.requestLocationUpdates(provider, 60000, 1, mylistener);
//		  } else {
//			  // leads to the settings because there is no last known location
////			  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
////			  startActivity(intent);
//		  }
		try {
			mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			mylistener = new MyLocationListener();
			String provider = mLocationManager.getBestProvider(criteria, false);
			Location location = mLocationManager.getLastKnownLocation(provider);
			// getting GPS status
			boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
                locationlayout.setVisibility(View.GONE);
				Toast.makeText(this, "Location service failed. Enable Location Service", Toast.LENGTH_LONG).show();
			} else {
				// First get location from Network Provider
				if (isNetworkEnabled) {
					if (mLocationManager != null) {
					mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 1, mylistener);
//					Toast.makeText(this, "Location service Network", Toast.LENGTH_LONG).show();
//					Log.d("Network", "Network");
//					if (mLocationManager != null) {
						location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
//							Toast.makeText(this, "Location service", Toast.LENGTH_LONG).show();
//							mylistener.onLocationChanged(location);
							String lat =String.valueOf(location.getLatitude());
							String longitude = String.valueOf(location.getLongitude());
							textViewLocation.setText(lat+", " +longitude,BufferType.NORMAL);
                            locationlayout.setVisibility(View.VISIBLE);
							LocationData locationData = new LocationData();
							locationData.setId(LogData.getInstance().getAppID(AppMain.getAppMainContext()));
							locationData.setLat(lat);
							locationData.setLongitude(longitude);
							sendLocationRequest(AppMain.getAppMainContext(), locationData);
//							lat = location.getLat();
//							lng = location.getLongitude();
						}else{
                            locationlayout.setVisibility(View.GONE);
                        }
					}
//				}else{
//					Toast.makeText(this, "No Location service Network", Toast.LENGTH_LONG).show();
				}
				//get the location by gps
				if (isGPSEnabled) {
					if (location == null) {
                        if (mLocationManager != null) {
                            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_LOCATION_UPDATES, 1, mylistener);
//						Log.d("GPS Enabled", "GPS Enabled");
//						if (mLocationManager != null) {
//                            Toast.makeText(this, "Location service GPS", Toast.LENGTH_LONG).show();
//							mylistener.onLocationChanged(location);
                            location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
//								Toast.makeText(this, "Location service GPS LOCATION", Toast.LENGTH_LONG).show();
                                String lat = String.valueOf(location.getLatitude());
                                String longitude = String.valueOf(location.getLongitude());
                                textViewLocation.setText(lat + ", " + longitude, BufferType.NORMAL);
                                LogData.getInstance().setLocation(getAppMainContext(), lat + "," + longitude);
                                LocationData locationData = new LocationData();
                                locationData.setId(LogData.getInstance().getAppID(getAppMainContext()));
                                locationData.setLat(lat);
                                locationData.setLongitude(longitude);
                                sendLocationRequest(getAppMainContext(), locationData);
                                locationlayout.setVisibility(View.VISIBLE);
                            } else {
                                locationlayout.setVisibility(View.GONE);
                            }
                        }
                    }
//				}else{
//					Toast.makeText(this, "no Location service GPS", Toast.LENGTH_LONG).show();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * THis function returns IMEI
	 * @return IMEI of the STB if exists
	 * @deprecated 
	 * 
	 */
//	String getIMEI(){
//		String imei = "";
//		try{
//			String ts = Context.TELEPHONY_SERVICE;
//			TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(ts);
//			imei = mTelephonyMgr.getDeviceId();
//		}catch (Exception e) {
//
//		}
//		return imei;
//	}



	/**
	 * In this method it is checked if the STB is already is registered
	 *  if yes then it is switched to display mode if layout exist else registration is checked.
	 *  @since version 1.0
	 */
	@TargetApi(17)
	private void startApp(){
		Toast tStatus;
		if(ClientConnectionConfig._DISPLAYNAME.equals("") || ClientConnectionConfig._HARDWAREKEY.equals("") ){
			tStatus = Toast.makeText(AppMain.this, "Enter valid display name", Toast.LENGTH_SHORT);
			tStatus.show();
		}else{
//			cancelPopup = true;
//			Display display = getWindowManager().getDefaultDisplay();
//			Point size = new Point();
//			if(currentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR1){
//				display.getRealSize(size);
//			}else{
//				display.getSize(size);
//			}
//			int layoutWidth = size.x;
//			int layoutHeight = size.y;
//			String resolution = ""+layoutWidth+"X"+layoutHeight;
			LogData.getInstance().setDisplayname(getAppMainContext(), ClientConnectionConfig._DISPLAYNAME);
			LogData.getInstance().setAppID(getAppMainContext(), ClientConnectionConfig._HARDWAREKEY);
			LogData.getInstance().setAddress(getAppMainContext(), ClientConnectionConfig._ADDRESS);
			LogData.getInstance().setAssetID(getAppMainContext(), ClientConnectionConfig._ASSETID);
			String xml = LogData.getInstance().getCurrentLayoutXml(getAppMainContext());
			
			if(xml!=null && !xml.equalsIgnoreCase("") &&!xml.equalsIgnoreCase(LogData.STR_UNKNOWN)){
				StateMachine.displayingContentInOffileMode = false;
				StateMachine.gi(AppMain.this).initProcess(false, StateMachine.OFFLINE);
			}else{
				StateMachine.displayingContentInOffileMode = true;
				StateMachine.gi(AppMain.this).initProcess(true, StateMachine.REGISTER);
			}
//			StateMachine.gi(AppMain.this).initProcess(true, StateMachine.REGISTER);
		}

	}

	private void refreshScreen(){
		try {
			if(refreshRunnable == null){
				refreshRunnable = new Runnable() {
					@Override
					public void run() {
						new InternetAccessible().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
						refreshHandler.postDelayed(refreshRunnable, _TIME_TO_REHIT_SERVER);
					}
				};
				refreshHandler =  new Handler();
				refreshHandler.postDelayed(refreshRunnable, _TIME_TO_REHIT_SERVER);
			}else{
				if(refreshHandler != null) {
					refreshHandler.postDelayed(refreshRunnable, _TIME_TO_REHIT_SERVER);
				}else{
					refreshHandler =  new Handler();
					refreshHandler.postDelayed(refreshRunnable, _TIME_TO_REHIT_SERVER);
				}
			}
		}catch (Exception e) {
			refreshScreen();
		}

	}

	/**
	 * In this method AlarmService is started for Update and Heartbeat service
	 * @since version 1.0
	 */
	private void startUpdater(){
		////Update service is registered
		//Toast.makeText(context, "Alarm manager available", Toast.LENGTH_LONG).show();
		Context context = getApplicationContext();
		setRecurringAlarm(context);
//		setHeartbeatAlarm(context);
		
	}
	
	/**
	 * Heartbeat alarm service is registered to send heartbeat to server every 120 seconds
	 * @param context application context
	 * @since version 1.0
	 * 
	 */
//	private void setHeartbeatAlarm(Context context) {
//		Calendar updateTime = Calendar.getInstance();
//
////		 Toast.makeText(context, "Alarm manager begin", Toast.LENGTH_LONG).show();
//		Intent downloader = new Intent(context, HeartBeatReceiver.class);
//		PendingIntent recurringHeartbeat = PendingIntent.getBroadcast(context,
//				0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
//		AlarmManager alarms = (AlarmManager) getSystemService(
//				Context.ALARM_SERVICE);
//		alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
//				updateTime.getTimeInMillis(),
//				HEARTBEAT_TIME, recurringHeartbeat);
////		 Toast.makeText(context, "Alarm manager started ", Toast.LENGTH_LONG).show();
//	}

	/**
	 * APK Update alarm service is registered to check for updates
	 *  and download and install every 300 Seconds
	 * @param context application Context
	 * @since version 1.0
	 */
	private void setRecurringAlarm(Context context) {
		Calendar updateTime = Calendar.getInstance();
//		updateTime.set(Calendar.HOUR_OF_DAY, 00);
//
//		updateTime.set(Calendar.MINUTE, 30);
		Intent downloader = new Intent(context, AlarmReceiver.class);
		PendingIntent recurringDownload = PendingIntent.getBroadcast(context,
				0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager alarms = (AlarmManager) getSystemService(
				Context.ALARM_SERVICE);
		alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				updateTime.getTimeInMillis(),
				AlarmManager.INTERVAL_FIFTEEN_MINUTES, recurringDownload);
	}



	/**
	 * In this method the display name is saved to database and
	 * registration process is started if not registered  else data is displayed in offline mode 
	 * @return true is data saved successfully elese false
	 * @since version 1.0
	 */
	private void saveData(){
		ClientConnectionConfig._ADDRESS = editBoxAddress.getText().toString().trim();
		ClientConnectionConfig._ASSETID = editBoxAssetId.getText().toString().trim();		
		ClientConnectionConfig._DISPLAYNAME = editBoxDisplayName.getText().toString().trim();
		
		if( ClientConnectionConfig._DISPLAYNAME.equals("")){
			Toast tStatus = Toast.makeText(AppMain.this, getString(R.string.enter_diplay_name), Toast.LENGTH_SHORT);
			tStatus.show();

		}else{
			LogData.getInstance().setDisplayname(getApplicationContext(),ClientConnectionConfig._DISPLAYNAME);
			LogData.getInstance().setAssetID(getApplicationContext(),ClientConnectionConfig._ASSETID);
			LogData.getInstance().setAppID(getApplicationContext(),ClientConnectionConfig._HARDWAREKEY);
//			if(!DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_DISPLAY_NAME, ClientConnectionConfig._DISPLAYNAME)){
//				dbSaveStatus = false;
//			}
//			if(!DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_ADDRESS, ClientConnectionConfig._ADDRESS)){
//				dbSaveStatus = false;
//			}
//			
//			if(!DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_HARDWARE_KEY, ClientConnectionConfig._HARDWAREKEY)){
//				dbSaveStatus = false;
//			}
			startApp();
		}

	}


	/**
	 * This method gets MacId of the STB
	 * @since version 1.0
	 */
	private void getMacaddress(){
		WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		String address;
		if(wifiManager.isWifiEnabled()) {
		    // WIFI ALREADY ENABLED. GRAB THE MAC ADDThis method HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    address = trimStringText(address, "%");
		    textViewMacId.setText(address, BufferType.NORMAL);
		} else {
		    // ENABLE THE WIFI FIRST
		    wifiManager.setWifiEnabled(true);

		    // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    address = trimStringText(address, "%");
		    textViewMacId.setText(address, BufferType.NORMAL);
		    wifiManager.setWifiEnabled(false);
		}
//		LogData.getInstance().setMacAddress(AppMain.getAppMainContext(), address);
		
	}
	
	/**
	 * In this method the string is trimmed till a specific string separator
	 * @param strToTrim string To Trim
	 * @param separator string separator
	 * @return trimmed string
	 * @since version 1.0
	 */
	private String trimStringText(String strToTrim, String separator){
		String temp = strToTrim;
		int indx = temp.indexOf(separator);
		if(indx > -1){
		 temp = temp.substring(0,indx);
		}
		
		return temp;
		
	}
	
	/**
	 * In this method IpAddress(IP4 or IP6) of the network connected is fetched
	 * @since version 1.0
	 */
	private String getIpAddress()
	  { 
		String address = "";
	          try {
	              for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
	                  NetworkInterface intf = en.nextElement();
	                  for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
	                      InetAddress inetAddress = enumIpAddr.nextElement();
	                     address = inetAddress.getHostAddress();
	                      if (!inetAddress.isLoopbackAddress()) {
	                    	 address = inetAddress.getHostAddress();
	                    	 address = trimStringText(address, "%");
//	                    	 LogData.getInstance().setIpAddress(AppMain.getAppMainContext(), address);
		                  		return address;
	                    	 
//	                    	  break;
	                      }
	                  }
	              }
	          } catch (Exception ex) {
	              Log.e("IP Address", ex.toString());
	          }
         	 address = trimStringText(address, "%");
//           		LogData.getInstance().setIpAddress(AppMain.getAppMainContext(), address);
	          return address;
	      }
	

	/**
	 * In this method the initial textview value is initialised
	 * @param androidID device id or androidID
	 * @since version 1.0
	 */
	private void setInitialValue(String androidID){
		initializeLocationManager();
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int layoutWidth = size.x;
		int layoutHeight = size.y;
		textViewResolution.setText("width = "+""+layoutWidth+"\t " +"height = "+""+layoutHeight,BufferType.NORMAL);
//		String regId = androidID;//Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
		
//		String verClientStr = DataCacheManager.getInstance().readSettingData(IDisplayLayout._KEY_XVIDIA_CLIENT_VERSION);
		textViewRegID.setText(androidID, BufferType.NORMAL);
		getMacaddress();
		textViewIpAddress.setText(getIpAddress(), BufferType.NORMAL);
//		LogData.getInstance().setNetworkType(AppMain.getAppMainContext(),NetworkUtil.getConnectivityStatusString(this));
		connectionStatus.setText(NetworkUtil.getConnectivityStatusString(this), BufferType.NORMAL);
	}

	private static Context context = null ;
	/**
	 * 
	 * @return returns AppMain Context
	 * @see AppMain
	 * @since version 1.0
	 */
	public static Context getAppMainContext() {
		return context;

	}


	public boolean noChange = false;
	public boolean downloadFailed = false;


	/**
	 * On Backpress a pop up is displayed for authentication to exit from the application
	 * @since version 1.0
	 */
	@Override
	public void onBackPressed() {
		try {
//		cancelPopup = true;
		sendAppStatusRequest(LogData.STB_ON);
//		initiatePopupWindow();

//		if(handler != null){
//			 handler.removeCallbacks(runnableBackground);
//			 handler = null;
//			}
		android.os.Process.killProcess(android.os.Process.myPid());
//		super.onBackPressed();
//		finish();
		} catch (Exception ex) {
		}
	}

	@Override
	protected void onPause() {
		try {
//			if(!cancelPopup){				
//				initiatePopupWindow();
//			}
//			boolean logFlag = LogData.getInstance().setAppStatus(getApplicationContext(),LogData.STB_ON);
//			
//			if (logFlag) {
//				sendLogRequest(
//						AppMain.getAppMainContext(),
//						LogUtility.getInstance().getAppStatusJSON(
//								AppMain.getAppMainContext()),
//						LogUtility.getAppStatusLogUrl());
//			}
			if ( Build.VERSION.SDK_INT >= 23 &&
					ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED &&
					ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

				if (mLocationManager != null) {
					mLocationManager.removeUpdates(mylistener);
				}
			}else{
				if (mLocationManager != null) {
					mLocationManager.removeUpdates(mylistener);
				}
			}
			if(!AppState.ACTION_ON_PAUSE){
				sendAppStatusRequest(LogData.STB_ON);
				AppState.ACTION_ON_PAUSE= true;
			}
//			LogData.getInstance().setAppStatus(getAppMainContext(), LogData.STB_ON);
//			DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_APP_STATE, LogData.STB_ON);
			super.onPause();
			unregisterReceiver(receiver);
		} catch (Exception ex) {
		}
	}
	@Override
	protected void onResume() {
		try {
//			if(!cancelPopup){				
//				initiatePopupWindow();
//			}
			receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
			registerReceiver(receiver, intentFilter);
			if (AppState.BACKPRESSED_SIGNAGE_SCREEN) {
				AppState.ACTION_CANCEL_TIMER = false;
				startTimer(TIMER_TIME);
			}
			if(!AppState.BACKPRESSED_SIGNAGE_SCREEN)
				sendAppStatusRequest(LogData.APP_ON);
//			LogData.getInstance().setAppStatus(getAppMainContext(), LogData.APP_ON);
//			DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_APP_STATE, LogData.APP_ON);
			super.onResume();
		} catch (Exception ex) {
		}
	}



	private void sendAppStatusRequest(String appStatus){
		boolean logFlag = LogData.getInstance().setAppStatus(getApplicationContext(),appStatus);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.ENGLISH);
		Date now = new Date();
		String  data =  formatter.format(now);
		boolean appState= false;
		if(appStatus.equalsIgnoreCase(LogData.APP_ON)){
			appState = true;
		}
		if (logFlag) {
			String id = LogData.getInstance().getAppID(getAppMainContext())+"_"+LogData.TAG_ONOFF_APP+"_"+now.getTime();
				OnOffTimeData object = new OnOffTimeData();
	        	object.setId(id);
	        	object.setBoxId(LogData.getInstance().getAppID(getAppMainContext()));
	        	if(appState){
		    		object.setOnTime(data);
		        	object.setOffTime("");
	        	}else{
	        		object.setOnTime("");
		        	object.setOffTime(data);
	        	}
				DataCacheManager.getInstance(AppMain.getAppMainContext()).saveOnOffTimeData(MySqliteHelper.TABLE_NAME_ONOFF_TIME_APP_TABLE, object);
			
				if(LogUtility.checkNetwork(AppMain.getAppMainContext())){
//					RequestQueue mRequestQue = VolleySingleton.getInstance().getRequestQueue();
					String url = new ServiceURLManager().getUrl(IAPIConstants.API_KEY_ONOFF_APP);
					JSONObject jsonRequest = null;
                    ObjectMapper mapper = new ObjectMapper();
                    String jsonRequestString = null;
                    try {
                        jsonRequestString = mapper.writeValueAsString(object);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
					try {
						jsonRequest = new JSONObject(jsonRequestString);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject response) {
							ObjectMapper mapper = new ObjectMapper();
							OnOffTimeData obj = null;
							try {
								obj = mapper.readValue(response.toString(), OnOffTimeData.class);
							} catch (IOException e) {
								e.printStackTrace();
							}
							String deviceId = obj.getId();
							if (!deviceId.isEmpty()) {
								DataCacheManager.getInstance(getAppMainContext()).removeOnOffTimeById(MySqliteHelper.TABLE_NAME_ONOFF_TIME_APP_TABLE, deviceId);
							}
						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
						}
					});
					VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
				}

		}
	}

	@Override
	protected void onRestart() {
		try {
//			if(!cancelPopup){				
//				initiatePopupWindow();
//			}
//			LogData.getInstance().setAppStatus(getAppMainContext(), LogData.APP_ON);
//boolean logFlag = LogData.getInstance().setAppStatus(getApplicationContext(),LogData.APP_ON);
//			
//			if (logFlag) {
//				sendLogRequest(
//						AppMain.getAppMainContext(),
//						LogUtility.getInstance().getAppStatusJSON(
//								AppMain.getAppMainContext()),
//						LogUtility.getAppStatusLogUrl());
//			}
			sendAppStatusRequest(LogData.APP_ON);
//			DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_APP_STATE, LogData.APP_ON);
			super.onRestart();
		} catch (Exception ex) {
		}
	}


     private void showProgress() {
       try {
           dialog = new Dialog(AppMain.this);
           dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
           LinearLayout.LayoutParams param =  new LinearLayout.LayoutParams(layoutWidth-100,ViewGroup.LayoutParams.WRAP_CONTENT);
           param.gravity = Gravity.CENTER;
           View popupView = getLayoutInflater().inflate(R.layout.progressdialog, null);
           dialog.setContentView(popupView,param);
		   dialog.setCancelable(false);

           cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
           dialogtext = (TextView) dialog.findViewById(R.id.tv1);
           pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);

           pb.setProgress(0);
           pb.setProgressDrawable(context.getResources().getDrawable(R.drawable.green_progress));
       } catch (Exception e) {
           e.printStackTrace();
       }
   }
//    public static void updateShowProgress(String file_path, String message) {
//        try {
//			cur_val.setText("Starting download...");
//            dialog.setTitle(message);
//
////            TextView text = (TextView) dialog.findViewById(R.id.tv1);
////            text.setText("Downloading file from ... " + file_path);
////            pb.setProgress(0);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

	/**
	 * TImer is initialised to run for a specific time
	 * @param milSec time for which the timer should run
	 * @since version 1.0
	 */
	private void startTimer(int milSec){
		try{
		if(handler != null){
			 handler.removeCallbacks(runnableBackground);
			 handler = null;
			}
				runnableBackground = new Runnable() {
			    	   @Override
			    	   public void run() {
						   ClientConnectionConfig._HARDWAREKEY =LogData.getInstance().getAppID(getApplicationContext());
						   if(!ClientConnectionConfig._HARDWAREKEY.isEmpty()&& !ClientConnectionConfig._HARDWAREKEY.equalsIgnoreCase(LogData.STR_UNKNOWN)){
			    				if(AppState.BACKPRESSED_SIGNAGE_SCREEN && !AppState.ACTION_CANCEL_TIMER) {
//			    					cancelPopup = true;
//			    					StateMachine.gi(AppMain.this).initProcess(true, StateMachine.REGISTER);
			    					startApp();
								}
			    			}else{
			    				registerDevice();
			    			}
			    			if(handler != null){
								 handler.removeCallbacks(runnableBackground);
								 handler = null;
								}
			    	   }
			    	};
			handler =  new Handler();
			handler.postDelayed(runnableBackground, milSec);
		}catch(Exception e){
			
		}
	}

//	void stopPackageProcess(final String packageName, final Context context) {
//
//		new Handler().postDelayed(new Runnable() {
//			@Override
//			public void run() {
//
//				final ActivityManager am = (ActivityManager) context
//						.getSystemService(Context.ACTIVITY_SERVICE);
//
//				List<ActivityManager.RunningAppProcessInfo> mTasks =am.getRunningAppProcesses();
//
//				for (int i = 0; i < mTasks.size(); i++)
//				{
//					String name = mTasks.get(i).processName;
////							.getComponent().getPackageName();
//
//					if(name.equals(packageName)) {
//						int id = mTasks.get(i).pid;
//						android.os.Process.killProcess(id);
////				ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//						am.killBackgroundProcesses(packageName);
//						break;
//					}
//				}
//			}
//		},5000);
//
//	}

	/**
	 * PopUp is initialised
	 * @since version 1.0
	 */
	private void initiatePopupWindow() {
		try {
			startTimer(TIMER_TIME);
//			if(!cancelPopup){
				// We need to get the instance of the LayoutInflater
				 View popupView = getLayoutInflater().inflate(R.layout.screen_popup, null);
				int width =500;
				int height = 550; //getResources().getDimensionPixelSize(R.dimen.seekbar_width);
				 pwindo = new PopupWindow(popupView,width, height, true);
	//			LayoutInflater inflater = (LayoutInflater) getActivity()
	//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//			View layout = inflater.inflate(R.layout.screen_popup,(ViewGroup) findViewById(R.id.popup_element));
	//			pwindo = new PopupWindow(layout, 300, 370, true);
				pwindo.showAtLocation(popupView, Gravity.CENTER, 0, 0);
				// If the PopupWindow should be focusable
				pwindo.setFocusable(true);
				userName = (EditText) popupView.findViewById(R.id.username);
				password = (EditText) popupView.findViewById(R.id.password);
				Button btnCancel = (Button) popupView.findViewById(R.id.button_cancel);
				Button btnSubmit = (Button) popupView.findViewById(R.id.button_submit);
				btnCancel.setOnClickListener(new OnClickListener() {
	
					@Override
					public void onClick(View v) {
						if(handler != null){
							 handler.removeCallbacks(runnableBackground);
							 handler = null;
							}
						pwindo.dismiss();
					}
				});
				btnSubmit.setOnClickListener(new OnClickListener() {
	
					@Override
					public void onClick(View v) {
						if(handler != null){
							 handler.removeCallbacks(runnableBackground);
							 handler = null;
							}
						String username = userName.getText().toString();
						String passwod = password.getText().toString();
						if((!username.isEmpty()&& username.equalsIgnoreCase("admin"))&&(!passwod.isEmpty()&& passwod.equalsIgnoreCase("admin"))){
							android.os.Process.killProcess(android.os.Process.myPid());
						}else{
							Toast.makeText(AppMain.this, "Enter valid username/password", Toast.LENGTH_SHORT).show();
						}
						pwindo.dismiss();
						
						
					}
				});
//				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private  boolean isInternetAccessible() {
		try {
			HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
			urlc.setRequestProperty("User-Agent", "Test");
			urlc.setRequestProperty("Connection", "close");
			urlc.setConnectTimeout(2000);
			urlc.connect();
			return (urlc.getResponseCode() >= 200&& urlc.getResponseCode() < 300);
		} catch (IOException e) {
			Log.e("AppMain", "Couldn't check internet connection", e);
		}

		return false;
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				// construct a string from the valid bytes in the buffer
				String readMessage = new String(readBuf, 0, msg.arg1);
				if(readMessage.equalsIgnoreCase("register")){
//					Toast.makeText(this, "Register wifi", Toast.LENGTH_LONG).show();
					registerDevice();
				}else if(readMessage.equalsIgnoreCase("refresh")){
					LogData.getInstance().setCurrentLayoutXml(getAppMainContext(), LogData.STR_UNKNOWN);
					SignageData.resetSignageData();
					DataCacheManager.getInstance(getAppMainContext()).clearSettingCache();
					registerDevice();
//					Toast.makeText(this, "Refresh", Toast.LENGTH_LONG).show();
				}else if(readMessage.startsWith("Details")){

					String name, addr, asset;
					String[] array = readMessage.split(Pattern.quote("@#$"));
					name = array[1];
					addr = array[2];
					asset = array[3];
					if(!name.equalsIgnoreCase("blank")){
						editBoxDisplayName.setText(name);
						FileManager.writeTextToFile(editBoxDisplayName.getText().toString(),AppState.FILE_NAME_BOX);
					}
					if(!addr.equalsIgnoreCase("blank")){
						editBoxAddress.setText(addr);
					}
					if(!asset.equalsIgnoreCase("blank")){
						editBoxAssetId.setText(asset);
						FileManager.writeTextToFile(editBoxAssetId.getText().toString(),AppState.FILE_NAME_ASSET);
					}

				}else if(readMessage.startsWith("root")){

					installNewApk();

				}else if(readMessage.startsWith("wifiSettings")){

					openWifiSetting();

				}else if(readMessage.startsWith("cast")){
					if (!LogData.getInstance().getScreenCastEnabled(getAppMainContext())) {
						LogData.getInstance().setScreenCastEnabled(getAppMainContext(), true);
						if (!isAppRunningInBackground()) {
							launchWfdApplication();
						}
						screenCastToggleButton.setText(getString(R.string.button_screen_cast_enabled));
						LogData.getInstance().setScreenCastEnabled(getAppMainContext(), true);
						startActivity(new Intent(AppMain.this, ScreenMirrorActivity.class));
					} else {
						LogData.getInstance().setScreenCastEnabled(getAppMainContext(), false);
						screenCastToggleButton.setText(getString(R.string.button_screen_cast_disabled));
					}
				}else if(readMessage.startsWith("save")){

					if(!editBoxAssetId.getText().toString().isEmpty()){
						ClientConnectionConfig._ASSETID = editBoxAssetId.getText().toString();
						LogData.getInstance().setAssetID(getAppMainContext(), ClientConnectionConfig._ASSETID);
						FileManager.writeTextToFile(ClientConnectionConfig._ASSETID,AppState.FILE_NAME_ASSET);
//	        		DataCacheManager.getInstance().saveSettingData(_KEY_XVIDIA_ASSETID, ClientConnectionConfig._ASSETID);

						editBoxAssetId.setKeyListener(null);
						btnSave.setVisibility(View.GONE);
					}else{
						Toast tStatus = Toast.makeText(AppMain.this, "Please enter asset ID", Toast.LENGTH_SHORT);
						tStatus.show();
					}

				}
				Log.d(TAG, readMessage);
//				(chatFragment).pushMessage("Buddy: " + readMessage);
				break;

			case MY_HANDLE:
				Object obj = msg.obj;
//				(chatFragment).setChatManager((ChatManager) obj);
				chatManager = (ChatManager)obj;

		}
		return true;
	}

	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
		Thread handler = null;
        /*
         * The group owner accepts connections using a server socket and then spawns a
         * client socket for every client. This is handled by {@code
         * GroupOwnerSocketHandler}
         */

		if (p2pInfo.isGroupOwner) {
			Log.d(TAG, "Connected as group owner");
			try {
				handler = new GroupOwnerSocketHandler(getHandler());
				handler.start();
				Toast.makeText(this, "Connected", Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				Log.d(TAG,
						"Failed to create a server thread - ");
				return;
			}
		} else {
			Log.d(TAG, "Connected as peer");
			handler = new ClientSocketHandler(
					(this).getHandler(),
					p2pInfo.groupOwnerAddress);
			handler.start();
			Toast.makeText(this, "Connected peer", Toast.LENGTH_LONG).show();
		}

//		chatFragment = new WiFiChatFragment();
//		getFragmentManager().beginTransaction()
//				.replace(R.id.container_root, chatFragment).commit();
//		statusTxtView.setVisibility(View.GONE);
	}


	/**
		 * Location Listener is registered.
		 * It is invoked whenever location is changed if location permission is granted
		 * @author Ravi@xvidia
		 *@since version 1.0
		 */
	  private class MyLocationListener implements LocationListener {
			
		  @Override
		  public void onLocationChanged(Location location) {
				  String lat =String.valueOf(location.getLatitude());
				  String longitude = String.valueOf(location.getLongitude());
			  textViewLocation.setText(lat+", " +longitude,BufferType.NORMAL);
			  boolean logFlag = LogData.getInstance().setLocation(getAppMainContext(),lat+"," +longitude);
			  
			if (logFlag) {
				if(!LogData.getInstance().getAppID(getAppMainContext()).equalsIgnoreCase(LogData.STR_UNKNOWN)) {
					LocationData locationData = new LocationData();
					locationData.setId(LogData.getInstance().getAppID(getAppMainContext()));
					locationData.setLat(lat);
					locationData.setLongitude(longitude);
					sendLocationRequest(getAppMainContext(), locationData);
				}
			}
		  }
	
		  @Override
		  public void onStatusChanged(String provider, int status, Bundle extras) {
			 
		  }
	
		  @Override
		  public void onProviderEnabled(String provider) {
			  
	
		  }
	
		  @Override
		  public void onProviderDisabled(String provider) {
			  
		  }
	  }

	private class InternetAccessible extends AsyncTask<Void, Void, Integer > {

		@Override
		protected Integer doInBackground(Void... arg0) {

			if (LogUtility.checkNetwrk(getAppMainContext())) {
				if(isInternetAccessible()) {
					return 1;
				}else{
					return 0;
				}
			} else{
				return -1;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			int colorCode;
			String message = NetworkUtil.getConnectivityStatusString(getAppMainContext());
			if(result == 1){
				colorCode = R.color.greenEnd;
			}else if (result == 0){
				colorCode = R.color.orange_color;
				message = message +" but no internet access";
			}else{
				colorCode = R.color.red_color;
			}
			connectionStatus.setTextColor(getResources().getColor(colorCode));
			connectionStatus.setText(message, BufferType.NORMAL);
		}
	}
	  
}