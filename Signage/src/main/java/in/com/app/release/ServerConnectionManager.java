package in.com.app.release;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import in.com.app.release.model.IAPIConstants;
import in.com.app.release.model.InventoryData;
import in.com.app.release.network.ServiceURLManager;
import in.com.app.release.network.VolleySingleton;
import in.com.app.release.storage.caching.sqlight.manager.DataCacheManager;
import in.com.app.release.utility.LogUtility;
import in.com.app.release.wsdl.XMDS;

/**
 * This class is wrapper for a asyncTask that checks if STB is registered on server in the background.
 * @author Ravi@Xvidia
 * @since version 1.0
 *
 */
class ServerConnectionManager extends
AsyncTask<String, Void, String> implements IDisplayLayout {

	private AppMain appmainInstance = null;
	private final String TAG = getClass().getCanonicalName();
	
	ServerConnectionManager(AppMain appmain){
		appmainInstance = appmain;		
	}
	
	@Override
	protected String doInBackground(String... params) {
		XMDS xmds = new XMDS(ClientConnectionConfig._SERVERURL);

		return xmds.RegisterDisplay(ClientConnectionConfig._UNIQUE_SERVER_KEY, ClientConnectionConfig._HARDWAREKEY,
				ClientConnectionConfig._DISPLAYNAME, ClientConnectionConfig._VERSION);
	}

	@Override
	protected void onPostExecute(String result) {			
		Log.d(TAG, "Got response " + result);
		boolean nextStepFlag = false;
		if (result==null) {
			StateMachine.gi(appmainInstance).initProcess(nextStepFlag, StateMachine.CONNECTION_ERROR);
		}else if (result.contains("Display is active and ready to start")) {
			DataCacheManager.getInstance(AppMain.getAppMainContext()).saveSettingData(_KEY_XVIDIA_STATE_REG_COMPLETE, FLAG_TRUE);
			nextStepFlag = true;
		}
		try{
			if(appmainInstance!=null && result != null){
				sendBoxNameAndAssetIdRequest(appmainInstance);
				StateMachine.gi(appmainInstance).initProcess(nextStepFlag, StateMachine.GETSCHEDULE);
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	/**
	 * This function sends the box data request to server
	 * @param ctx
	 */
	private void sendBoxNameAndAssetIdRequest(Context ctx){
		try {
			InventoryData inventoryData = new InventoryData();
			inventoryData.setAssetId(ClientConnectionConfig._ASSETID);
			inventoryData.setBoxId(ClientConnectionConfig._HARDWAREKEY);
//			inventoryData.setBoxName(ClientConnectionConfig._DISPLAYNAME);
			if(LogUtility.checkNetwork(ctx)){
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestString = null;
				try {
					jsonRequestString = mapper.writeValueAsString(inventoryData);
				} catch (IOException e) {
					e.printStackTrace();
				}
				JSONObject jsonRequest = null;
				try {
					jsonRequest = new JSONObject(jsonRequestString);
				} catch (JSONException e) {
//                          e.printStackTrace();
				}
				final String url =new ServiceURLManager().getUrl(IAPIConstants.API_KEY_BOX_INVENTORY_DATA);
				JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

//                        Log.e("VolleyError ", error.getMessage());
					}
				});
				VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
			}
		}catch(Exception e){

		}
	}
}