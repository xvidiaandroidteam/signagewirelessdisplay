package in.com.app.release.network;


import in.com.app.release.MyApplication;
import in.com.app.release.R;
import in.com.app.release.model.IAPIConstants;


public class ServiceURLManager implements IAPIConstants {

	private final String base_URL = MyApplication.getAppContext().getResources().getString(R.string.service_base_url);
	public String getURL(int SERVICE_API){
		String service = "";
		switch (SERVICE_API) {
			case API_KEY_MEDIA_TIME:
				service = base_URL + ServiceURL.API_KEY_MEDIA_TIME;
				break;
			case API_KEY_MEDIA_TIME_OFFLINE:
				service = base_URL + ServiceURL.API_KEY_MEDIA_TIME_OFFLINE;
				break;
			case API_KEY_LAYOUT_TIME:
				service = base_URL + ServiceURL.API_KEY_LAYOUT_TIME;
				break;
			case API_KEY_LAYOUT_TIME_OFFLINE:
				service = base_URL + ServiceURL.API_KEY_LAYOUT_TIME_OFFLINE;
				break;
			case API_KEY_ONOFF_SCREEN:
				service = base_URL + ServiceURL.API_KEY_ONOFF_SCREEN;
				break;
			case API_KEY_ONOFF_APP:
				service = base_URL + ServiceURL.API_KEY_ONOFF_APP;
				break;
			case API_KEY_ONOFF_BOX:
				service = base_URL + ServiceURL.API_KEY_ONOFF_BOX;
				break;
			case API_KEY_ONOFF_SCREEN_OFFLINE:
				service = base_URL + ServiceURL.API_KEY_ONOFF_SCREEN_OFFLINE;
				break;
			case API_KEY_ONOFF_APP_OFFLINE:
				service = base_URL + ServiceURL.API_KEY_ONOFF_APP_OFFLINE;
				break;
			case API_KEY_ONOFF_BOX_OFFLINE:
				service = base_URL + ServiceURL.API_KEY_ONOFF_BOX_OFFLINE;
				break;
			case API_KEY_BOX_LOCATION:
				service = base_URL + ServiceURL.API_KEY_BOX_LOCATION;
				break;
			case API_KEY_BOX_INVENTORY_DATA:
				service = base_URL + ServiceURL.API_KEY_BOX_INVENTORY_DATA;
				break;
			case API_KEY_BOX_GET_INVENTORY_DATA:
				service = base_URL + ServiceURL.API_KEY_BOX_GET_INVENTORY_DATA;
				break;
			case API_KEY_BOX_DOWNLOADING_STATUS:
				service = base_URL + ServiceURL.API_KEY_BOX_DOWNLOADING_STATUS;
				break;
			case API_KEY_BOX_UPDATE_APK:
				service = base_URL + ServiceURL.API_KEY_BOX_UPDATE_APK;
				break;
		}
		return service;	
	}




	public String getUrl(int urlKey){
		return getURL(urlKey);
	}

	public String getDownloadBaseUrl(){
		String idea = "http://ideasignagerepo.xvidiaglobal.com/";

//		String idea = "http://192.168.1.16/my-repo/";
		return idea;
	}


}
